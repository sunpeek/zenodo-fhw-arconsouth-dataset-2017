"""Utilities used by script `make_plots.py`.
"""
import numpy as np
import pandas as pd
from pathlib import Path

import utils_data as udt
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.patches as mpatches
from matplotlib.colors import LinearSegmentedColormap, ListedColormap, to_rgba
import matplotlib.ticker as ticker
import matplotlib.transforms as transforms

# Configuration

FILE_EXTENSION = '.jpg'
FULL_WIDTH = 16.59 / 2.54  # full page width, cm to inches
EXPORT_FOLDER = Path(__file__).with_name('plots')

DAY_START = pd.to_datetime('2017-04-12 00:00').tz_localize(udt.TIMEZONE)
DAY_END = pd.to_datetime('2017-04-13 00:00').tz_localize(udt.TIMEZONE)

# Publication quality plt settings inspired by http://aeturrell.com/2018/01/31/publication-quality-plots-in-python/
FONT_SIZE = 8
plt.rcParams.update({
    'figure.dpi': 600,
    'font.family': 'Times New Roman',
    'font.size': FONT_SIZE,
    'text.usetex': False,
    'axes.labelsize': FONT_SIZE,
    'axes.titlesize': FONT_SIZE,
    'axes.labelpad': 2,
    'axes.titlepad': 4,
    'axes.linewidth': 0.5,
    'xaxis.labellocation': 'left',
    'grid.alpha': 0.5,
    'grid.linewidth': 0.5,
    'lines.linewidth': 0.75,
    'patch.linewidth': 0.25,
    'legend.fontsize': FONT_SIZE - 1,
    'legend.title_fontsize': FONT_SIZE - 1,
    'legend.framealpha': 0.8,
    'legend.borderpad': 0.2,
    'legend.columnspacing': 1.5,
    'legend.labelspacing': 0.25,
    'xtick.labelsize': FONT_SIZE - 1,
    'ytick.labelsize': FONT_SIZE,
    'xtick.major.size': 2,
    'xtick.major.pad': 1,
    'xtick.major.width': 0.5,
    'ytick.major.size': 2,
    'ytick.major.pad': 1,
    'ytick.major.width': 0.5,
    'xtick.minor.size': 2,
    'xtick.minor.pad': 1,
    'xtick.minor.width': 0.5,
    'ytick.minor.size': 2,
    'ytick.minor.pad': 1,
    'ytick.minor.width': 0.5,
})

# Color: matplotlib style 'tableau-colorblind10'
# Style: https://viscid-hub.github.io/Viscid-docs/docs/dev/styles/tableau-colorblind10.html
# Color names https://stackoverflow.com/questions/74830439/list-of-color-names-for-matplotlib-style-tableau-colorblind10
COLORS = {
    'blue': '#5F9ED1',
    'sky_blue': '#006BA4',
    'sail_blue': '#A2C8EC',
    'pumpkin': '#FF800E',
    'orange': '#C85200',
    'cheese': '#FFBC79',
    'gray': '#595959',
    'warm_gray': '#898989',
    'light_gray': '#CFCFCF',
    'dark_gray': '#ABABAB',
    'almost_black': '#373737',
}
COLORS['missing_data'] = COLORS['light_gray']

# Avoid black unless necessary
# Taken from https://atchen.me/research/code/data-viz/2022/01/04/plotting-matplotlib-reference.html
plt.rcParams.update({
    'text.color': COLORS['almost_black'],
    'patch.edgecolor': COLORS['gray'],
    'patch.force_edgecolor': True,
    'hatch.color': COLORS['almost_black'],
    'axes.edgecolor': COLORS['almost_black'],
    'axes.labelcolor': COLORS['almost_black'],
    'xtick.color': COLORS['almost_black'],
    'ytick.color': COLORS['almost_black']
})


def read_data():  # -> pd.DataFrame
    """Read full dataset, output by `make_data.py`. Return full DataFrame and DataFrame filtered for day plots.
    """
    df_full = udt.read_processed_data()

    # unit conversions (Kelvin to °C)
    temp_columns = [column for column in df_full.columns if ("te_" in column and "__std" not in column)]
    df_full[temp_columns] -= 273.15

    # unit conversions (m³/s to m³/h)
    vf_columns = [column for column in df_full.columns if "vf" in column]
    df_full[vf_columns] *= 3600

    # unit conversions (W to kW)
    tp_columns = [column for column in df_full.columns if "tp_" in column]
    df_full[tp_columns] /= 1000

    df_day = df_full.copy()
    df_day = df_day[df_day.index >= DAY_START]
    df_day = df_day[df_day.index <= DAY_END]

    return df_full, df_day


def format_xaxis(ax, date_start=DAY_START, date_end=DAY_END,
                 minor_locator=mdates.HourLocator(interval=1),
                 major_locator=mdates.HourLocator(interval=2),
                 major_formatter=mdates.DateFormatter("%#H:%M", tz=udt.TIMEZONE)):
    ax.set_xlim(date_start, date_end, auto=None)
    ax.xaxis_date(udt.TIMEZONE)
    ax.xaxis.set_minor_locator(minor_locator)
    ax.xaxis.set_major_locator(major_locator)
    ax.xaxis.set_major_formatter(major_formatter)
    adjust_xlabel_positions(ax, 4, remove_last=True)

    # Position extra text below x axis
    vertical_offset_points = 10
    offset = transforms.ScaledTranslation(0, vertical_offset_points / 72, ax.figure.dpi_scale_trans)
    ax.text(0, 0, f'{date_start:%Y-%m-%d} (UTC+1)', ha='left',
            transform=ax.transAxes - offset, va='top', fontsize=plt.rcParams['axes.labelsize'] - 1)

    return ax


def adjust_xlabel_positions(ax, move_left_points, remove_last=False):
    """Align x axis labels left, but shift slightly to the left.
    """
    if remove_last:
        ax.set_xlabel(ax.get_xticklabels()[-1].set_visible(False))
    # Move all label texts slightly left
    offset = transforms.ScaledTranslation(move_left_points / 72, 0, ax.figure.dpi_scale_trans)
    for lbl in ax.xaxis.get_majorticklabels():
        lbl.set_horizontalalignment('left')
        lbl.set_transform(lbl.get_transform() - offset)


def save_fig(filename, export_folder=EXPORT_FOLDER):
    Path(export_folder).mkdir(exist_ok=True)
    plt.savefig(export_folder.joinpath(filename).with_suffix(FILE_EXTENSION))
    plt.close()
    print(f'Saved plot "{filename}"')


def plot_operation__day(df, filename='Fig2_operation__day'):
    """Plot temperatures, power, volume flow.
    """
    fig, ax = plt.subplots(3, sharex=True, height_ratios=[5, 5, 2])
    fig.subplots_adjust(left=.15, bottom=.16, right=.99, top=.97)
    twin_x = ax[1].twinx()

    # plot data (Temperatures)
    ax[0].plot(df['te_out'], "-", label="te_out", color=COLORS['orange'], zorder=20)
    # ax[0].plot(df['te_out'], "-", label="te_out", color=COLORS['dark_gray'], zorder=20)
    ax[0].plot(df['te_out_row1'], ":", label="te_out_row1", alpha=0.8, color=COLORS['light_gray'], zorder=5)
    ax[0].plot(df['te_out_row2'], ":", label="te_out_row2", alpha=0.8, color=COLORS['gray'], zorder=5)
    ax[0].plot(df['te_out_row3'], ":", label="te_out_row3", alpha=0.8, color=COLORS['cheese'], zorder=5)
    ax[0].plot(df['te_out_row4'], ":", label="te_out_row4", alpha=0.8, color=COLORS['dark_gray'], zorder=5)

    ax[0].plot(df['te_in'], "-", label="te_in", color=COLORS['sky_blue'], zorder=10)
    ax[0].plot(df['te_amb'], "-", label="te_amb", color=COLORS['cheese'], zorder=30)

    # plot data (Irradiation)
    ax[1].plot(df['rd_gti'], label="rd_gti (left axis)", color=COLORS['orange'])

    # plot data (Calculated Power)
    twin_x.plot(df['tp__calc'], label="tp__calc (right axis)", color=COLORS['warm_gray'])

    # plot data (Volume Flow)
    ax[2].fill_between(df.index, 0, df['vf'], label="vf",
                       color=COLORS['gray'], linewidth=plt.rcParams['grid.linewidth'], alpha=0.4)

    # formatting (axis labels)
    ax[0].set_ylabel("Temperature [°C]")
    ax[1].set_ylabel("Irradiance [W/m²]")
    ax[2].set_ylabel("Volume flow \n[m³/h]")
    twin_x.set_ylabel("Thermal power [kW]")

    # formatting (limits)
    ax[0].set_ylim(0, 100)
    ax[1].set_ylim(0, 1000)
    ax[2].set_ylim(0, 10)
    twin_x.set_ylim(0, 500)
    ax[1].set_yticks(ticks=np.linspace(0, 1000, num=6))
    twin_x.set_yticks(ticks=np.linspace(0, 500, num=6))

    # Axes formatting
    ax_names = ['(a) Temperatures collector array and ambient',
                '(b) Irradiance and thermal power',
                '(c) Volume flow']
    for i, v in enumerate(ax_names):
        leg = ax[i].legend(loc="upper right")
        leg.set_zorder(50)
        ax[i].set_title(v, loc='center')
        ax[i].grid()
        ax[i].set_axisbelow('line')

    # combine twinx and ax[1] labels into one legend item
    lines, labels = ax[1].get_legend_handles_labels()
    lines2, labels2 = twin_x.get_legend_handles_labels()
    ax[1].legend(lines + lines2, labels + labels2, loc="upper right")

    # layout
    fig.tight_layout()
    fig.set_size_inches(w=FULL_WIDTH, h=FULL_WIDTH / 1.4)
    format_xaxis(ax[-1])

    save_fig(filename)


def plot_irradiance__day(df, filename='Fig3_irradiance__day'):
    """Plot various irradiances (beam, diffuse, global / horizontal, tilted).
    """
    fig, ax = plt.subplots(3, sharex=True, gridspec_kw={'height_ratios': [1, 1, 1]})

    color_tilted = COLORS["orange"]
    color_horizontal = COLORS["dark_gray"]
    color_dni = COLORS["cheese"]

    # plot data (Global Irradiation)
    ax[0].plot(df['rd_gti'], color=color_tilted, label="rd_gti", zorder=10)
    ax[0].plot(df['rd_ghi'], color=color_horizontal, label="rd_ghi", zorder=5)

    # plot data (Direct Irradiation)
    # ax[1].plot(df['rd_dni'], '-', color=color_dni, label="rd_dni", alpha=0.5)
    ax[1].plot(df['rd_dni'], '-', color=color_dni, label="rd_dni", zorder=3)
    ax[1].plot(df['rd_bti__calc'], color=color_tilted, label="rd_bti__calc", zorder=10)
    ax[1].plot(df['rd_bhi__calc'], color=color_horizontal, label="rd_bhi__calc", zorder=5)

    # Plot data (Diffuse Irradiation)
    ax[2].plot(df['rd_dti__calc'], color=color_tilted, label="rd_dti__calc", zorder=10)
    ax[2].plot(df['rd_dhi__calc'], color=color_horizontal, label="rd_dhi__calc", zorder=5)

    # formatting (label)
    for ax_ in ax:
        ax_.set_ylabel("Irradiance [W/m²]")

    # Axes formatting
    ax_names = ['(a) Global irradiance', '(b) Beam irradiance', '(c) Diffuse irradiance']
    y_lims = [1000, 685, 685]
    for i, v in enumerate(ax_names):
        ax[i].legend(loc="upper right")
        ax[i].set_title(v, loc='center')
        ax[i].set_ylim(0, y_lims[i])
        n_ticks = 1 + np.floor(y_lims[i] / 200)
        ax[i].set_yticks(ticks=np.arange(n_ticks) * 200)
        ax[i].grid()
        ax[i].set_axisbelow('line')

    fig.set_size_inches(w=FULL_WIDTH, h=FULL_WIDTH / 1.4)
    fig.tight_layout()
    format_xaxis(ax[-1])

    save_fig(filename)


def plot_shadows__year(df, filename='Fig4_shadowing__year', only_internal_shadow=False):
    """Plot various shadowing channels (internal & external shading, shadow share).
    """
    colors = dict(
        horizon="#696863",
        external=COLORS['cheese'],
        shadowed=COLORS['pumpkin'],
    )

    # Calculations and data preparation
    df["sun_above_horizon"] = (df["sun_apparent_elevation__calc"] > 0).astype(float)
    df.loc[df["sun_apparent_elevation__calc"].isna(), "sun_above_horizon"] = np.nan

    day_index = pd.date_range(start=df.index.min(), end=df.index.max(), freq="1D").date
    minute_of_day = 60 * df.index.hour + df.index.minute

    def pivoting(column):
        pivoted = pd.pivot_table(df, values=column, index=df.index.date, columns=minute_of_day)
        pivoted = pivoted.reindex(day_index)
        return pivoted.transpose().iloc[::-1]

    # Color Scales
    cmap_horizon = LinearSegmentedColormap.from_list('sun', [colors['horizon'], "none"])
    cmap_external = LinearSegmentedColormap.from_list('external', [colors['external'], "white"])
    cmap_shadow = LinearSegmentedColormap.from_list('shadowed', [colors['shadowed'], "none"])
    cmap_shadowed_share = plt.cm.PuBu

    # plotting
    fig, ax = plt.subplots()
    ax.set_facecolor(COLORS['missing_data'])
    im_kwargs = dict(aspect='auto', interpolation='none',
                     extent=[mdates.date2num(day_index[0]), mdates.date2num(day_index[-1]), 0, 24])

    if only_internal_shadow:
        im = ax.imshow(pivoting('rd_bti_shadowed_share__calc'), cmap=cmap_shadowed_share, alpha=0.8, **im_kwargs)
        ax.legend(loc="upper right",
                  handles=[mpatches.Patch(facecolor=COLORS['missing_data'], label="missing data")])
        filename = filename + '_internal'
    else:
        ax.imshow(pivoting('is_shadowed_external'), cmap=cmap_external, **im_kwargs)
        ax.imshow(pivoting("is_shadowed__calc"), cmap=cmap_shadow, **im_kwargs)
        im = ax.imshow(pivoting('rd_bti_shadowed_share__calc'), cmap=cmap_shadowed_share, alpha=0.5, **im_kwargs)
        ax.imshow(pivoting("sun_above_horizon"), cmap=cmap_horizon, **im_kwargs)

        ax.legend(loc="upper right", handles=[
            mpatches.Patch(facecolor=colors['shadowed'], label="is_shadowed__calc == False", alpha=0.5),
            mpatches.Patch(facecolor=colors['external'], label="is_shadowed_external == False", alpha=0.5),
            mpatches.Patch(facecolor=colors['horizon'], label="sun_apparent_elevation__calc < 0"),
            mpatches.Patch(facecolor=COLORS['missing_data'], label="missing data"),
        ])

    cbar = plt.colorbar(im)
    cbar.ax.set_title('rd_bti_shadowed_share__calc', pad=8, ha='right', x=3.5)
    cbar.ax.tick_params(labelsize=plt.rcParams['xtick.labelsize'])

    # formatting
    ax.xaxis_date()
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%b'))
    adjust_xlabel_positions(ax, move_left_points=2)

    ax.set_yticks([0, 6, 12, 18, 24])
    ax.set_yticklabels(["0:00", "6:00", "12:00", "18:00", "24:00"], fontsize=plt.rcParams['xtick.labelsize'])
    ax.set_ylabel("Time of day (UTC+1)", fontsize=plt.rcParams['xtick.labelsize'])

    # saving
    fig.tight_layout()
    fig.set_size_inches(w=FULL_WIDTH, h=FULL_WIDTH / 2)

    save_fig(filename)


def plot_uncertainty__day(df, filename='Fig5_uncertainty__day'):
    fig, ax = plt.subplots(3, 2, sharex=True)
    CI_FACTOR = 1.96
    filter_idx = (760, 811)
    df_filtered = df[filter_idx[0]:filter_idx[1]].copy()
    date_start = df_filtered.index[0]
    date_end = df_filtered.index[-1]
    df_filtered['cp_in__calc'] /= 1000
    df_filtered['cp_in__calc__std'] /= 1000

    def plot_column(ax, column, ylabel, tick_spacing, figname, color=COLORS['almost_black'], y_lims=None):
        y = df_filtered[column]
        y_ci = CI_FACTOR * df_filtered[column + "__std"]

        ax.plot(y, color=color, label=column, zorder=5)
        ax.fill_between(df_filtered.index, y - y_ci, y + y_ci, color='black', alpha=0.2, edgecolor=None,
                        label='95% CI', zorder=2)
        ax.grid()
        ax.legend(loc="upper right")
        ax.set_title(figname, loc='center')
        ax.set_ylabel(ylabel)
        ax.set_xlim(date_start, date_end, auto=None)
        if y_lims is not None:
            ax.set_ylim(y_lims)
        ax.yaxis.set_major_locator(ticker.MultipleLocator(tick_spacing))
        ax.set_axisbelow('line')
        return

    plot_names = ['(a) Thermal power', '(b) Volume flow',
                  '(c) Inlet temperature', '(d) Outlet temperature',
                  '(e) Density', '(f) Heat capacity']
    plot_column(ax[0, 0], column="tp__calc", ylabel="Thermal power [kW]",
                tick_spacing=20, color='red', figname=plot_names[0])
    plot_column(ax[0, 1], column="vf", ylabel="Volume flow [m³/h]",
                tick_spacing=0.2, y_lims=[8.0, 8.9], figname=plot_names[1])
    plot_column(ax[1, 0], column="te_in", ylabel="Temperature [°C]",
                tick_spacing=2, y_lims=[58.8, 67], figname=plot_names[2])
    plot_column(ax[1, 1], column="te_out", ylabel="Temperature [°C]",
                tick_spacing=2, y_lims=[82.8, 91], figname=plot_names[3])
    plot_column(ax[2, 0], column="rho_in__calc", ylabel="Density [kg/m³]",
                tick_spacing=40, y_lims=[950, 1090], figname=plot_names[4])
    plot_column(ax[2, 1], column="cp_in__calc", ylabel="Heat capacity [kJ / (kg*K)]",
                tick_spacing=0.2, y_lims=[3.41, 4.35], figname=plot_names[5])

    fig.tight_layout(pad=1.0)
    fig.set_size_inches(w=FULL_WIDTH, h=FULL_WIDTH / 1.1)
    locator = mdates.MinuteLocator(interval=10)
    for ax_ in [ax[-1, 0], ax[-1, 1]]:
        format_xaxis(ax_, date_start=date_start, date_end=date_end,
                     minor_locator=locator, major_locator=locator)

    save_fig(filename)

def plot_data_availability(df, filename='Fig6_data_availabilty'):
    """Plot when we have data and when we have NaNs.
    """
    fig, ax = plt.subplots(1, 1)
    color_data_ok = COLORS['blue']

    # Encode NaN and valid data as 0 | 1
    x = df['te_in'].copy()
    x.where(x.isna(), 1, inplace=True)
    x[x.isna()] = 0

    # Plot
    x_lims = mdates.date2num([x.index[0], x.index[-1]])
    ax.imshow(np.expand_dims(x.to_numpy(), axis=0),
              extent=[x_lims[0], x_lims[1], 0, 1],
              aspect=40, cmap=ListedColormap([COLORS['missing_data'], color_data_ok]))

    # Format axes
    ax.get_yaxis().set_ticks([])
    ax.xaxis_date(udt.TIMEZONE)
    ax.xaxis.set_major_locator(mdates.MonthLocator())
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%b', tz=udt.TIMEZONE))
    adjust_xlabel_positions(ax, move_left_points=2)

    # Create legend
    patches = [mpatches.Patch(facecolor=color_data_ok, label="Data available"),
               mpatches.Patch(facecolor=COLORS['missing_data'], label="Data missing")]
    ax.legend(handles=patches, loc="upper right")

    fig.tight_layout()
    fig.set_size_inches(w=FULL_WIDTH, h=FULL_WIDTH / 6)

    save_fig(filename)