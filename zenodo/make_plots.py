"""
This file is part of a data set for solar thermal collector array data shared on zenodo.

This script reads the processed data and generates the plots shown in the Data-In-Brief article.

.. codeauthor:: Lukas Feierl <l.feierl@solid.at>
.. codeauthor:: Philip Ohnewein <p.ohnewein@aee.at>
.. codeauthor:: Peter Zauner <p.zauner@solid.at>
"""
import utils_plots as upl

# Parse Data
df, df_day = upl.read_data()

# Operation, line plot
upl.plot_operation__day(df_day)

# Irradiance, line plot
upl.plot_irradiance__day(df_day)

# Shadows, heatmap plot
upl.plot_shadows__year(df)
# upl.plot_shadows__year(df, only_internal_shadow=True)

# Uncertainty
upl.plot_uncertainty__day(df_day)

# Data availability
upl.plot_data_availability(df)

