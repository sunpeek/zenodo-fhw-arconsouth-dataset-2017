"""
This file is part of a dataset for solar thermal collector array data shared on zenodo.

This script reads measured data channels of the collector array, and produces a set of calculated channels.
- The measured data channels (plus uncertainties) are read from the csv file FHW_ArcS__meas__2017.csv.
- The calculated channels (plus uncertainties) are exported to the csv file FHW_ArcS__full__2017.csv.
- Auxiliary information for the calculations is read from a JSON file, FHW_ArcS__parameters.json.
- Physical units of all channels are included in the above JSON file and in the variable `unit`.

List of calculated data channels (see FHW_ArcS__parameters.json for details):
- `mf__calc` : Mass flow
- `tp__calc` : Thermal power
- `rho_in__calc`, `rho_out__calc` : Heat transfer fluid density at inlet | outlet of collector array
- `cp_in__calc`, `cp_out__calc` : Heat transfer fluid heat capacity at inlet | outlet of collector array
- `rd_bti__calc` : Beam irradiance in collector plane
- `rd_bhi__calc` : Beam horizontal irradiance
- `rd_dti__calc` : Diffuse irradiance in collector plane
- `rd_dhi__calc` : Diffuse horizontal irradiance
- `aoi__calc` : Angle of incidence between the normal vector of the collector plane and the sun-beam vector.
- `sun_azimuth__calc`, `sun_apparent_elevation__calc` : Solar azimuth and apparent elevation. Uncertainty ignored.
- `is_shadowed__calc` : True if collector array is affected by internal or external shading.
- `is_shadowed_internal__calc` : True if collector array is affected by internal (row-to-row) shading.
- `rd_bti_shadowed_share__calc` : Degree of beam shading due to row-to-row shading, float in 0...1. Uncertainty ignored.


.. codeauthor:: Philip Ohnewein <p.ohnewein@aee.at>
.. codeauthor:: Daniel Tschopp <d.tschopp@aee.at>
.. codeauthor:: Marnoch Hamilton-Jones <m.hamilton-jones@aee.at>
"""

import utils_data as udt

# Read raw / measured csv data, with NA (not available) filtered out (these are re-inserted later)
df_raw, full_index = udt.read_raw_data()
# Build uncertainty objects of the variables needed in this script
uarr = udt.get_uncertainty_objects(df_raw)
# Dictionary with pint-readable unit string, provided for convenience
units = udt.get_unit_strings()

# Calculation of data channels -----------------------------------------------------------------------------------------

# Calculate fluid properties at inlet and outlet
uarr['rho_in'], uarr['rho_out'] = udt.calc_fluid_prop('density', df_raw['te_in'], df_raw['te_out'])
uarr['cp_in'], uarr['cp_out'] = udt.calc_fluid_prop('heat_capacity', df_raw['te_in'], df_raw['te_out'])

# Calculate mass flow and thermal power
uarr['mf'] = udt.calc_mf_from_vf(uarr['vf'], uarr['rho_in'])
uarr['tp'] = udt.calc_tp_from_mf(uarr['mf'], uarr['cp_in'], uarr['cp_out'], uarr['te_in'], uarr['te_out'])

# Calculate solar angles
sun_azimuth, sun_apparent_elevation, sun_zenith = udt.calc_solar_angles(timestamps=df_raw.index, te_amb=uarr['te_amb'])
aoi = udt.calc_aoi(azimuth=sun_azimuth, zenith=sun_zenith)

# Calculate beam and diffuse radiations on horizontal and collector plane
uarr['rd_bhi'], uarr['rd_dhi'] = udt.calc_radiations_horizontal(uarr['rd_ghi'], uarr['rd_dni'], sun_apparent_elevation)
uarr['rd_bti'], uarr['rd_dti'] = udt.calc_radiations_tilted(uarr['rd_gti'], uarr['rd_dni'], aoi)

# Calculate shading
rd_bti_shadowed_share = udt.calc_shadowing(sun_azimuth, sun_apparent_elevation, aoi)
is_shadowed_internal = 1 * (rd_bti_shadowed_share > 0)
is_shadowed = 1 * ((is_shadowed_internal + df_raw['is_shadowed_external']) > 0)

# Calculation of data channels done ------------------------------------------------------------------------------------

# Compile raw / measured and calculated data channels into DataFrame:
# Add calculated channels _without_ uncertainty information
df = df_raw.assign(**{
    'aoi__calc': aoi,
    'sun_azimuth__calc': sun_azimuth,
    'sun_apparent_elevation__calc': sun_apparent_elevation,
    'is_shadowed_internal__calc': is_shadowed_internal,
    'is_shadowed__calc': is_shadowed,
    'rd_bti_shadowed_share__calc': rd_bti_shadowed_share})
# Add calculated channels _with_ uncertainty information
uncertainty_channel_names = ['mf', 'tp', 'rho_in', 'rho_out', 'cp_in', 'cp_out', 'rd_bti', 'rd_bhi', 'rd_dti', 'rd_dhi']
df = udt.add_channels_with_uncertainty(df, uncertainty_array=uarr, channel_names=uncertainty_channel_names)

# Reorder columns to match order in JSON file
df = df[udt.get_parameters()['data_channels'].keys()]
# Re-insert original NaN rows
df_full = df.reindex(full_index)
# Save data as csv and parquet
udt.save_processed_data(df_full)

# To read the exported csv file into pandas for further analysis, use:
# df = utils_data.read_processed_data()
