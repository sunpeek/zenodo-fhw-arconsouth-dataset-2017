"""Utilities used by script `make_data.py`.
"""
import json
import numpy as np
import pandas as pd
import pytz
from pathlib import Path

import pvlib as pv
from uncertainties import unumpy, ufloat
from scipy.constants import zero_Celsius
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import PolynomialFeatures, StandardScaler
from sklearn.linear_model import Ridge
from sklearn.model_selection import GridSearchCV, ShuffleSplit

CSV_PROCESSED_PATH = Path(__file__).with_name('FHW_ArcS__main__2017.csv')
PARQUET_PROCESSED_PATH = Path(__file__).with_name('FHW_ArcS__main__2017.parquet')
CSV_MEASURED_PATH = Path(__file__).with_name('FHW_ArcS__raw__2017.csv')
PARAMS_PATH = Path(__file__).with_name('FHW_ArcS__parameters.json')

TIMEZONE = pytz.FixedOffset(60)  # like 'Europe/Vienna', but without daylight-saving time (DST)
MAXIMUM_SHARE_BEAM_TO_GLOBAL_RADIATION = 0.93  # Used in calculation of beam and diffuse radiations


def read_raw_data():
    """Return the raw measurements (including standard deviations) as a pandas DataFrame.
    All columns share the same NaN pattern. These NaN rows are filtered out, the NaN mask is returned for later use.
    """
    df = pd.read_csv(CSV_MEASURED_PATH, encoding='utf-8', sep=';',
                     on_bad_lines='skip', parse_dates=False, dtype='str')
    # Convert to UTC+1, local time without DST (daylight saving time, "summer time")
    df['timestamps'] = pd.to_datetime(df['timestamps_UTC'], utc=True).dt.tz_convert(TIMEZONE)
    df = df.set_index('timestamps')
    df = df.drop(columns='timestamps_UTC')
    df = df.apply(pd.to_numeric, errors='coerce')
    nan_mask = df[df.columns[0]].isna()

    return df.loc[~nan_mask, :], nan_mask.index


def read_processed_data():
    """Read parsed data (from csv or parquet file) and return as DataFrame.
    """
    try:
        df = pd.read_parquet(PARQUET_PROCESSED_PATH)
    except ModuleNotFoundError:
        df = pd.read_csv(CSV_PROCESSED_PATH, encoding='utf-8', sep=';',
                         parse_dates=["timestamps"], index_col="timestamps")
    return df


def save_processed_data(df_full):
    """Export entire data set (measured + calculated channels) to csv file.
    """
    df_full.to_csv(CSV_PROCESSED_PATH, encoding='utf-8', sep=';')
    try:
        df_full.to_parquet(PARQUET_PROCESSED_PATH)
    except ModuleNotFoundError:
        print('Parquet file not saved: Saving processed data to parquet file requires optional dependency pyarrows.')


def get_uncertainty_objects(df):
    """Return dictionary of unumpy uncertainty-aware arrays, for variables needed in calculations.
    """
    skip_column = lambda s: s.endswith('__std') or s == 'is_shadowed_external'
    return {s: unumpy.uarray(df[s], df[s + '__std']) for s in df.columns if not skip_column(s)}


def get_parameters():
    """Read FHW_collector_array_parameters.json, return dictionary.
    """
    with open(PARAMS_PATH) as f:
        conf = json.load(f)
    return conf


def get_unit_strings():
    params = get_parameters()
    return {k: params['data_channels'][k]["unit"] for k in params['data_channels'].keys()}


def add_channels_with_uncertainty(df, uncertainty_array, channel_names):
    """Given dictionary of unumpy arrays, adds nominal values and standard deviations to DataFrame columns.
    """
    for channel_name in channel_names:
        df[channel_name + '__calc'] = unumpy.nominal_values(uncertainty_array[channel_name])
        df[channel_name + '__calc__std'] = unumpy.std_devs(uncertainty_array[channel_name])
    return df


def _get_fluid_model(prop_str):
    """Return trained model and error for fluid density or heat capacity, based on laboratory measurements (see JSON).
    """
    params = get_parameters()
    X = [[zero_Celsius + x] for x in
         params['collector_array']['fluid']['fluid_' + prop_str]['temperature']['magnitude']]
    y = params['collector_array']['fluid']['fluid_' + prop_str][prop_str]['magnitude']

    pipe = make_pipeline(StandardScaler(), PolynomialFeatures(), Ridge(alpha=1e-3))
    grid = GridSearchCV(pipe,
                        {'polynomialfeatures__degree': np.arange(2, 4)},
                        cv=ShuffleSplit(n_splits=10),
                        scoring='neg_mean_absolute_error')

    grid.fit(X, y)
    model = grid.best_estimator_
    max_error = params['collector_array']['fluid']['fluid_' + prop_str]['max_error']

    return model, max_error


def calc_fluid_prop(prop_str, te_in, te_out):
    """Return fluid density or heat capacity (prop_str) at collector array inlet and outlet, including uncertainty.
    """
    assert prop_str in ['density', 'heat_capacity']
    # Get trained fluid model and error information
    model, max_error = _get_fluid_model(prop_str)
    # Convert maximum error / uniform distribution to standard deviation of normal distribution, see GUM
    ci2sd = lambda ci: ci / np.sqrt(3)

    te = {'in': [[x] for x in te_in], 'out': [[x] for x in te_out]}
    prop = {}
    for k, X in te.items():
        prop_ = model.predict(X)
        prop[k] = unumpy.uarray(prop_, ci2sd(prop_ * max_error))

    return prop['in'], prop['out']


def calc_mf_from_vf(vf, rho):
    """Calculate mass flow from volume flow, density at volume flow measurement and heat capacity.
    """
    mf = vf * rho
    return mf


def calc_tp_from_mf(mf, cp_in, cp_out, te_in, te_out):
    """Calculate thermal power from mass flow, inlet and outlet temperatures.
    """
    cp = 0.5 * (cp_in + cp_out)
    tp = mf * cp * (te_out - te_in)
    return tp


def calc_solar_angles(timestamps, te_amb):
    """Calculate sun azimuth and apparent elevation.
    """
    params = get_parameters()
    # Ambient temperature uncertainty effect on solar angles is ignored
    sol_pos = pv.solarposition.get_solarposition(time=timestamps,
                                                 latitude=params['plant_information']['latitude']['magnitude'],
                                                 longitude=params['plant_information']['longitude']['magnitude'],
                                                 altitude=params['plant_information']['altitude']['magnitude'],
                                                 temperature=unumpy.nominal_values(te_amb))
    return sol_pos['azimuth'].to_numpy(), \
        sol_pos['apparent_elevation'].to_numpy(), \
        sol_pos['apparent_zenith'].to_numpy()


def calc_aoi(azimuth, zenith):
    """Calculate angle of incidence between the collectors' normal vector and the sun-beam vector.
    """
    params = get_parameters()
    aoi = pv.irradiance.aoi(surface_tilt=params['collector_array']['tilt']['magnitude'],
                            surface_azimuth=params['collector_array']['azimuth']['magnitude'],
                            solar_zenith=zenith,
                            solar_azimuth=azimuth)
    return aoi


def calc_radiations_horizontal(rd_ghi, rd_dni, sun_elevation):
    """Calculate horizontal beam and diffuse irradiances.
    """
    rd_bhi = rd_dni * np.cos(np.deg2rad(90 - sun_elevation))
    # Set beam radiation to zero when sun is below the horizon
    sun_below_horizon = (sun_elevation < 0) & ~np.isnan(unumpy.nominal_values(rd_bhi))
    rd_bhi = np.ma.array(rd_bhi, mask=sun_below_horizon).filled(ufloat(0, 0))

    # Set beam radiation to zero when beam share is unplausibly large. This correction is necessary because of the
    # distance (about 50 meters) between the pyrheliometer and the global radiation sensor at the plant location.
    # rd_ghi > 1 is used to avoid numeric problems with np.divide
    beam_share = np.divide(rd_bhi, rd_ghi,
                           out=unumpy.uarray(np.zeros_like(rd_bhi), np.zeros_like(rd_bhi)), where=rd_ghi > 1)
    beam_too_large = (beam_share > MAXIMUM_SHARE_BEAM_TO_GLOBAL_RADIATION) \
                     & ~np.isnan(unumpy.nominal_values(rd_bhi)) \
                     & ~np.isnan(unumpy.nominal_values(rd_ghi))
    rd_bhi = np.ma.array(rd_bhi, mask=beam_too_large).filled(
        np.ma.array(rd_ghi, mask=beam_too_large) * MAXIMUM_SHARE_BEAM_TO_GLOBAL_RADIATION)

    rd_dhi = rd_ghi - rd_bhi

    return rd_bhi, rd_dhi


def calc_radiations_tilted(rd_gti, rd_dni, aoi):
    """Calculate beam and diffuse irradiances in plane of collector array.
    """
    rd_bti = rd_dni * np.cos(np.deg2rad(aoi))
    # Set beam radiation to zero when sun is behind collector
    sun_behind_collector = (aoi > 90) & ~np.isnan(unumpy.nominal_values(rd_bti))
    rd_bti = np.ma.array(rd_bti, mask=sun_behind_collector).filled(ufloat(0, 0))

    # Set beam radiation to zero when beam share is unplausibly large. This correction is necessary because of the
    # distance (several meters) between the pyrheliometer and the global radiation sensor at the plant location.
    # rd_ghi > 1 is used to avoid numeric problems with np.divide
    beam_share = np.divide(rd_bti, rd_gti,
                           out=unumpy.uarray(np.zeros_like(rd_bti), np.zeros_like(rd_bti)), where=rd_gti > 1)
    beam_too_large = (beam_share > MAXIMUM_SHARE_BEAM_TO_GLOBAL_RADIATION) \
                     & ~np.isnan(unumpy.nominal_values(rd_bti)) \
                     & ~np.isnan(unumpy.nominal_values(rd_gti))
    rd_bti = np.ma.array(rd_bti, mask=beam_too_large).filled(
        np.ma.array(rd_gti, mask=beam_too_large) * MAXIMUM_SHARE_BEAM_TO_GLOBAL_RADIATION)

    rd_dti = rd_gti - rd_bti

    return rd_bti, rd_dti


def calc_shadowing(azimuth, elevation, aoi):
    """Calculates share of internal (row-to-row) shading, based on a 2D geometrical approach taken from:
    Bany, J. and Appelbaum, J. (1987): "The effect of shading on the design of a field of solar collectors",
        Solar Cells 20, p. 201 - 228, :doi:`https://doi.org/10.1016/0379-6787(87)90029-9`
    """
    params = get_parameters()
    sun_behind_collector = (aoi > 90)
    sun_below_horizon = (elevation < 0)

    # Paper formula (18), nomenclature according to BANY and APPELBAUM (1987)
    beta = np.deg2rad(params['collector_array']['tilt']['magnitude']
                      - params['collector_array']['ground_tilt']['magnitude'])
    sb = np.sin(beta)
    cb = np.cos(beta)
    A = params['collector_model']['gross_width']['magnitude']
    Hc = A * np.sin(beta)
    D = params['collector_array']['row_spacing']['magnitude'] - A * cb

    # Relative collector spacing:
    Drel = D / Hc
    gamma = np.deg2rad(azimuth - params['collector_array']['azimuth']['magnitude'])
    alpha = np.deg2rad(elevation)

    # hs: shadow height [0..1]
    cg = np.cos(gamma)
    hs = 1 - ((Drel * sb + cb) / (cb + sb * cg / np.tan(alpha)))
    hs[hs > 1] = 1
    hs[hs < 0] = 0
    hs[sun_behind_collector | sun_below_horizon] = 1
    rd_bti_shadowed_share = hs

    return rd_bti_shadowed_share
