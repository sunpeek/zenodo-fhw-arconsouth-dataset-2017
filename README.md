# Zenodo FHW ArconSouth Dataset 2017

This project is intended as a companion to support the [zenodo repository](https://zenodo.org/record/7741084).  
The goal of this project is to facilitate maintenance of the Python code provided along with the data. 

- This dataset is independent of the [SunPeek](https://gitlab.com/sunpeek/sunpeek/) software. A subset of 
  this dataset (same collector array and time interval, but without measurement uncertainties) has been 
  released as [example data](https://gitlab.com/sunpeek/example-data/) for direct use with the 
  [SunPeek](https://gitlab.com/sunpeek/sunpeek/) software.  

 
## How to work with this repository
- To run the Python code, we advise to use the [Poetry](https://python-poetry.org/) dependency 
  manager with the provided `pyproject.toml` file. 
- For a detailed description of the data files, see the [zenodo repository](https://zenodo.org/record/7741084). 
- To read this dataset into pandas for further analysis, use:
  ```
  import utils_data as udt
  df = udt.read_processed_data()
  ```

## Contributing

- If you think about contributing to this project, you are very welcome. Please follow the steps described in the 
[SunPeek Contributing Guidelines](https://gitlab.com/sunpeek/sunpeek/-/blob/main/CONTRIBUTING.md).
 
 

This package is licensed under a 
[Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).
